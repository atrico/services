package filesystem

import "os"

type FileSystemService interface {
	// Stat returns a FileInfo describing the named file.
	// If there is an error, it will be of type *PathError.
	Stat(path string) (info os.FileInfo, err error)
	// Mkdir creates a directory named path,
	// along with any necessary parents, and returns nil,
	// or else returns an error.
	// The permission bits perm (before umask) are used for all
	// directories that MkdirAll creates.
	// If path is already a directory, MkdirAll does nothing
	// and returns nil.
	Mkdir(path string, perm os.FileMode) (err error)
	// Create creates or truncates the named file. If the file already exists,
	// it is truncated. If the file does not exist, it is created with mode 0666
	// (before umask). If successful, methods on the returned File can
	// be used for I/O; the associated file descriptor has mode O_RDWR.
	// If there is an error, it will be of type *PathError.
	Create(path string) (file File, err error)
	// Open opens the named file for reading. If successful, methods on
	// the returned file can be used for reading; the associated file
	// descriptor has mode O_RDONLY.
	// If there is an error, it will be of type *PathError.
	Open(path string) (file FileRead, err error)
	// OpenFile is the generalized open call; most users will use Open
	// or Create instead. It opens the named file with specified flag
	// (O_RDONLY etc.). If the file does not exist, and the O_CREATE flag
	// is passed, it is created with mode perm (before umask). If successful,
	// methods on the returned File can be used for I/O.
	// If there is an error, it will be of type *PathError.
	OpenFile(name string, flag int, perm os.FileMode) (file File, err error)
}

var OsFileSystemService FileSystemService = osFileSystemService{}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
type osFileSystemService struct{}

func (o osFileSystemService) Stat(path string) (info os.FileInfo, err error) {
	return os.Stat(path)
}

func (o osFileSystemService) Mkdir(path string, perm os.FileMode) (err error) {
	return os.MkdirAll(path, perm)
}

func (o osFileSystemService) Open(path string) (file FileRead, err error) {
	return wrapFile(func() (*os.File, error) { return os.Open(path) })
}

func (o osFileSystemService) Create(path string) (file File, err error) {
	return wrapFile(func() (*os.File, error) { return os.Create(path) })
}

func (o osFileSystemService) OpenFile(path string, flag int, perm os.FileMode) (file File, err error) {
	return wrapFile(func() (*os.File, error) { return os.OpenFile(path, flag, perm) })
}

func wrapFile(create func() (*os.File, error)) (file File, err error) {
	var osFile *os.File
	if osFile, err = create(); err == nil {
		file = fileWrapper{osFile}
	}
	return file, err
}
