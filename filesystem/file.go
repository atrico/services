package filesystem

import (
	"io"
	"os"
)

type FileRead interface {
	io.ReadCloser
	io.ReaderAt
	io.Seeker
	// Stat returns the FileInfo structure describing file.
	// If there is an error, it will be of type *PathError.
	Stat() (os.FileInfo, error)
	// ReadDir reads the contents of the directory associated with the file f
	// and returns a slice of DirEntry values in directory order.
	// Subsequent calls on the same file will yield later DirEntry records in the directory.
	//
	// If n > 0, ReadDir returns at most n DirEntry records.
	// In this case, if ReadDir returns an empty slice, it will return an error explaining why.
	// At the end of a directory, the error is io.EOF.
	//
	// If n <= 0, ReadDir returns all the DirEntry records remaining in the directory.
	// When it succeeds, it returns a nil error (not io.EOF).
	ReadDir(n int) ([]os.DirEntry, error)
}

type File interface {
	FileRead
	io.WriteCloser
	io.WriterAt
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
type fileWrapper struct {
	*os.File
}
