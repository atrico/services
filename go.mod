module gitlab.com/atrico/services

go 1.19

require (
	gitlab.com/atrico/core v1.27.3
	gitlab.com/atrico/testing/v2 v2.3.1
)

require golang.org/x/exp v0.0.0-20221108223516-5d533826c662 // indirect
