package time

import "time"

type TimeService interface {
	// Now returns the current local time.
	Now() time.Time
	// After waits for the duration to elapse and then sends the current time
	// on the returned channel.
	// It is equivalent to NewTimer(d).C.
	// The underlying Timer is not recovered by the garbage collector
	// until the timer fires. If efficiency is a concern, use NewTimer
	// instead and call Timer.Stop if the timer is no longer needed.
	After(d time.Duration) <-chan time.Time
}

var OsTimeService TimeService = timeService{}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
type timeService struct{}

func (t timeService) Now() time.Time {
	return time.Now()
}

func (t timeService) After(duration time.Duration) <-chan time.Time {
	return time.After(duration)
}