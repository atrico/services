package unit_tests

import (
	"gitlab.com/atrico/core"
	"gitlab.com/atrico/services/filesystem"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"gitlab.com/atrico/testing/v2/random"
	"io"
	"os"
	"path/filepath"
	"testing"
)

var rg = random.NewValueGenerator()

func TestMain(m *testing.M) {
	tmpDir = core.Must2(os.MkdirTemp, "", "")
	defer os.Remove(tmpDir)
	os.Exit(m.Run())
}

var tmpDir string
var svc = filesystem.OsFileSystemService

func Test_FS_Stat(t *testing.T) {
	// Arrange
	filename, contents := osCreateFileWithContents()

	// Act
	info, err := svc.Stat(filename)

	// Assert
	assert.That[any](t, err, is.Nil, "stat error")
	assertStat(t, info, filepath.Base(filename), int64(len(contents)), is.False)
}

func Test_FS_Stat_MissingFile(t *testing.T) {
	// Arrange
	var filename = createFilename()

	// Act
	_, err := svc.Stat(filename)

	// Assert
	assert.That(t, os.IsNotExist(err), is.True, "not exist error")
}

func Test_FS_MkDir(t *testing.T) {
	// Arrange
	parts := []string{rg.Identifier(), rg.Identifier(), rg.Identifier()}
	path1 := filepath.Join(tmpDir, parts[0], parts[1])
	path2 := filepath.Join(path1, parts[2])

	// Act
	err1 := svc.Mkdir(path1, 0755)
	err2 := svc.Mkdir(path2, 0755)

	// Assert
	assert.That[any](t, err1, is.Nil, "subpath error")
	assert.That[any](t, err2, is.Nil, "fullpath error")
	fileInfo, err := os.Stat(path2)
	assert.That[any](t, err, is.Nil, "dir exists")
	assert.That(t, fileInfo.IsDir(), is.True, "is a dir")
}

func Test_FS_Create(t *testing.T) {
	// Arrange
	filename := createFilename()
	contents := rg.StringOfLen(rg.IntBetween(100, 500))

	// Act & Assert
	// Create file
	file, err := svc.Create(filename)
	assert.That[any](t, err, is.Nil, "create error")
	assertFileStat(t, file, filepath.Base(filename), int64(0))
	// Write contents
	n, err2 := file.Write([]byte(contents))
	assert.That[any](t, err2, is.Nil, "write error")
	assert.That(t, n, is.EqualTo(len(contents)), "write length")
	file.Close()
	osAssertFileContents(t, filename, contents)
}

func Test_FS_Create_ExistingFile(t *testing.T) {
	// Arrange
	filename, _ := osCreateFileWithContents()

	// Act
	file, err := svc.Create(filename)

	// Assert
	assert.That[any](t, err, is.Nil, "create error")
	assertFileStat(t, file, filepath.Base(filename), 0)
}

func Test_FS_Open(t *testing.T) {
	// Arrange
	filename, contents := osCreateFileWithContents()

	// Act & Assert
	// Open file
	file, err := svc.Open(filename)
	assert.That[any](t, err, is.Nil, "open error")
	defer file.Close()
	assertFileStat(t, file, filepath.Base(filename), int64(len(contents)))
	// Read contents
	assertFileContents(t, file, contents)
}

func Test_FS_Open_MissingFile(t *testing.T) {
	// Arrange
	var filename = createFilename()

	// Act
	_, err := svc.Open(filename)

	// Assert
	assert.That(t, os.IsNotExist(err), is.True, "not exist error")
}

func Test_FS_OpenFile_ForRead(t *testing.T) {
	// Arrange
	filename, contents := osCreateFileWithContents()

	// Act & Assert
	// Open file
	file, err := svc.OpenFile(filename, os.O_RDONLY, 0)
	assert.That[any](t, err, is.Nil, "open error")
	defer file.Close()
	assertFileStat(t, file, filepath.Base(filename), int64(len(contents)))
	// Read contents
	assertFileContents(t, file, contents)
}

func Test_FS_OpenFile_ForWrite(t *testing.T) {
	// Arrange
	filename, contents := osCreateFileWithContents()
	extraContent := rg.StringOfLen(rg.IntBetween(10, 100))

	// Act & Assert
	// Open file
	file, err := svc.OpenFile(filename, os.O_APPEND, 0)
	assert.That[any](t, err, is.Nil, "open error")
	defer file.Close()
	assertFileStat(t, file, filepath.Base(filename), int64(len(contents)))
	// Write more content
	n, err2 := file.Write([]byte(extraContent))
	assert.That[any](t, err2, is.Nil, "write error")
	assert.That(t, n, is.EqualTo(len(extraContent)), "write length")
	file.Close()
	osAssertFileContents(t, filename, contents+extraContent)
}

func Test_FS_OpenFile_MissingFile(t *testing.T) {
	// Arrange
	var filename = filepath.Join(tmpDir, rg.Identifier())

	// Act
	_, err := svc.OpenFile(filename, os.O_RDONLY, 0)

	// Assert
	assert.That(t, os.IsNotExist(err), is.True, "not exist error")
}

func createFilename() string {
	return filepath.Join(tmpDir, rg.Identifier())
}

func osCreateFileWithContents() (filename string, contents string) {
	filename = createFilename()
	file := core.Must1(os.Create, filename)
	defer file.Close()
	contentSize := rg.IntBetween(50, 500)
	contents = rg.StringOfLen(contentSize)
	file.WriteString(contents)
	return filename, contents
}

func osAssertFileContents(t *testing.T, filename string, contents string) {
	file := core.Must1(os.Open, filename)
	defer file.Close()
	assertFileContentsImpl(t, file, contents)
}

func assertFileStat(t *testing.T, file filesystem.FileRead, name string, size int64) {
	assertFsObjectStat(t, file, name, size, is.False)
}

func assertDirStat(t *testing.T, file filesystem.FileRead, name string) {
	assertFsObjectStat(t, file, name, 0, is.True)
}

func assertFsObjectStat(t *testing.T, file filesystem.FileRead, name string, size int64, isDir assert.Matcher[bool]) {
	info, err := file.Stat()
	assert.That[any](t, err, is.Nil, "stat error")
	assertStat(t, info, name, size, isDir)
}

func assertStat(t *testing.T, info os.FileInfo, name string, size int64, isDir assert.Matcher[bool]) {
	assert.That(t, info.Name(), is.EqualTo(name), "name")
	assert.That(t, info.IsDir(), isDir, "is dir")
	assert.That(t, info.Size(), is.EqualTo(size), "size")
}

func assertFileContents(t *testing.T, file filesystem.FileRead, contents string) {
	assertFileContentsImpl(t, file, contents)
}

func assertFileContentsImpl(t *testing.T, file io.Reader, contents string) {
	buffer := make([]byte, len(contents))
	n, err := file.Read(buffer)
	assert.That[any](t, err, is.Nil, "read error")
	assert.That(t, n, is.EqualTo(len(contents)), "read length")
	assert.That(t, string(buffer), is.EqualTo(contents), "content")
	_, err = file.Read(buffer)
	assert.That(t, err, is.EqualTo(io.EOF), "nothing left in file")
}
